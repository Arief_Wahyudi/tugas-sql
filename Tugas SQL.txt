Soal 1 Membuat Database
 
Buatlah database dengan nama “myshop”. Tulislah di text jawaban pada nomor 1.

CREATE DATABASE myshop;

Soal 2 Membuat Table di Dalam Database

Table users
CREATE TABLE users ( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, email varchar(255) NOT null, password varchar(255) NOT null );

Table categories
CREATE TABLE categories ( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null );

Table items
CREATE TABLE items( id int(8) PRIMARY KEY AUTO_INCREMENT, name varchar(255) NOT null, description varchar(255) NOT null, price int(8) NOT null, stock int(8) NOT null, category_id int(8), FOREIGN KEY(category_id) REFERENCES categories(id) );

Soal 3 Memasukkan Data pada Table

Insert categories
INSERT INTO categories(name) VALUES ("gadget"),("cloth"),("men"),("women"),("branded");

Insert users
INSERT INTO users(name, email, password) VALUES("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");

Insert items
INSERT INTO items(name,description,price,stock,category_id) VALUES("Sumsang b50","hape keren dari merk sumsang","4000000","100","1"),("Uniklooh","baju keren dari brand ternama","500000","50","2"),("IMHO Watch","jam tangan anak yang jujur banget","2000000","10","1");